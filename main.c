#include <stdio.h>
#include <SDL2/SDL.h>
#include <math.h>
#include <assert.h>

#define INTERSECT_IMPLEMENTATION
#include "./intersect.h"

#define UNPACK_COLOUR(c) (c >> 8 * 3) & 0xFF, (c >> 8 * 2) & 0xFF, (c >> 8 * 1) & 0xFF, (c >> 8 * 0) & 0xFF
#define COLOUR_TO_SDLCOLOR(c) ((SDL_Color) {.r = (c >> 8 * 3) & 0xFF, .g = (c >> 8 * 2) & 0xFF, .b = (c >> 8 * 1) & 0xFF, .a = (c >> 8 * 0) & 0xFF})
#define BACKGROUND 0x181818FF
#define WALL 0xAAAAAAFF

int width = 800;
int height = 600;

void sc(int code, char* action) {
    if (code != 0) {
        fprintf(stderr, "ERROR %s: %s! Aborting!\n", action != NULL ? action : "", SDL_GetError());
        exit(1);
    }
}

void* scp(void* res, char* action) {
    if (res == NULL) {
        fprintf(stderr, "ERROR %s: %s! Aborting!\n", action != NULL ? action : "", SDL_GetError());
        exit(1);
    }
    return res;
}

void clear(SDL_Renderer* r) {
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(BACKGROUND));
    SDL_RenderClear(r);
}

// NOTE: Potentially slow. Use texture to speed up.
void draw_circle(SDL_Renderer* r, Vec center, float radius) {
    for (float dx = -radius; dx <= radius; ++dx) {
        for (float dy = -radius; dy <= radius; ++dy) {
            if ((dx*dx + dy*dy) <= radius*radius) {
                SDL_RenderDrawPoint(r, center.x + dx, center.y + dy);
            }
        }
    }
}

#define WALLS_COUNT 4
// RAY_COUNT must be odd
#define RAY_COUNT 21

#define RAY_ANGLE (M_PI / 3.0)
#define RAY_LEN 100.0f

typedef struct {
    Seg walls[WALLS_COUNT];
    Vec center;
    Vec rays[RAY_COUNT];
    Vec sight_lines[RAY_COUNT];
    float total_rotation;
} State;

State s = {0};

void update_sight_lines() {
    Seg segment = seg(s.center, vec(0.0f, 0.0f));
    for (int i = 0; i < RAY_COUNT; ++i) {
        segment.b = s.rays[i];
        bool hit = false;
        float min_scale = FLT_MAX;
        for (int j = 0; j < WALLS_COUNT; ++j) {
            float scale = FLT_MAX;
            if (intersect(segment, s.walls[j], &scale, CT_RAY)) {
                min_scale = scale < min_scale ? scale : min_scale;
                hit = true;
            }
        }
        s.sight_lines[i] = hit ? vscale(segment.b, min_scale) : vec(NAN, NAN);
    }
}

void rotate_view(float theta) {
    s.total_rotation = fmodf(s.total_rotation + theta, 2*M_PI);
    for (int i = 0; i < RAY_COUNT; ++i) {
        s.rays[i] = vrotate(s.rays[i], theta);
    }
}

void init_state() {
    assert(WALLS_COUNT == 4);
    int offset = 100;
    s.walls[0] = seg(vec(offset, offset), vec(width - 2*offset, 0));
    s.walls[1] = seg(vec(width - offset, offset), vec(0, height - 2*offset));
    s.walls[2] = seg(vec(width - offset, height - offset), vec(-(width - 2*offset), 0));
    s.walls[3] = seg(vec(offset, height - offset), vec(0, -(height - 2*offset)));

    s.center = vec((float) width/2, (float) height/2);

    double theta = -(RAY_ANGLE / 2);
    assert(RAY_COUNT > 1);
    double d_theta = RAY_ANGLE / (double) (RAY_COUNT - 1); // the minus one is so the middle ray is flat
    for (int i = 0; i < RAY_COUNT; ++i) {
        s.rays[i] = vec(RAY_LEN * cos(theta), RAY_LEN*sin(theta));
        theta += d_theta;
    }

    for (int i = 0; i < RAY_COUNT; ++i) {
        s.sight_lines[i] = vec(NAN, NAN);
    }
}

#define WALL_COLOR 0x00FF00FF
#define WALL_HEIGHT 200
#define HEAD_HEIGHT (WALL_HEIGHT / 10)

void draw_minimap(SDL_Renderer* r) {
    #define MINIMAP_SCALE (1.0f / 4.0f)
    SDL_Rect rect = (SDL_Rect) { .x = 0, .y = 0, .w = width*MINIMAP_SCALE,.h=height*MINIMAP_SCALE};
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(BACKGROUND));
    SDL_RenderFillRect(r, &rect);

    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(WALL_COLOR));
    for (int i = 0; i < WALLS_COUNT; ++i) {
        Seg w = sscale(s.walls[i], MINIMAP_SCALE);
        SDL_RenderDrawLine(r, w.a.x, w.a.y, endp(w).x, endp(w).y);
    }

    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(0x444444FF));
    for (int i = 0; i < RAY_COUNT; ++i) {
        if (isnan(s.sight_lines[i].x)) {
            Seg ray = sscale(seg(s.center, s.rays[i]), MINIMAP_SCALE);
            SDL_RenderDrawLine(r, ray.a.x, ray.a.y, endp(ray).x, endp(ray).y);
        } else {
            Seg ray = sscale(seg(s.center, s.sight_lines[i]), MINIMAP_SCALE);
            SDL_RenderDrawLine(r, ray.a.x, ray.a.y, endp(ray).x, endp(ray).y);
            #define HIT_CIRCLE_RADIUS 7
            SDL_SetRenderDrawColor(r, UNPACK_COLOUR(0xFFFFFFFF));
            draw_circle(r, endp(ray), HIT_CIRCLE_RADIUS * MINIMAP_SCALE);
            SDL_SetRenderDrawColor(r, UNPACK_COLOUR(0x444444FF));
        }
    }

    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(0xAA4444FF));
    #define CENTER_RADIUS 10
    draw_circle(r, vscale(s.center, MINIMAP_SCALE), CENTER_RADIUS * MINIMAP_SCALE);

    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(0xFFFFFFFF));
    int points[5][2] = {{0, 0}, {0, height * MINIMAP_SCALE}, {width * MINIMAP_SCALE, height * MINIMAP_SCALE}, {width * MINIMAP_SCALE, 0}, {0, 0}};
    SDL_RenderDrawLines(r, (SDL_Point*) points, 5);
}

float maxf(float a, float b) {
    return a > b ? a : b;
}

float minf(float a, float b) {
    return a > b ? b : a;
}

float interpolate(float prev_min, float prev_max, float t, float new_min, float new_max) {
    return minf(maxf(new_min, (t - prev_min) / (prev_max - prev_min) * (new_max - new_min) + new_min), new_max);
}

void render(SDL_Renderer* r) {
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(WALL_COLOR));

    float screen_x = 0.0f;
    assert(RAY_COUNT > 1);
    float delta_x = (float) width / (RAY_COUNT - 1);
    Seg prev_wall = seg(vec(0.0f, 0.0f), vec(0.0f, 0.0f));
    int prev_alpha = 0.0;
    bool walls_connected = false;

    for (int i = 0; i < RAY_COUNT; ++i) {
        Vec line = s.sight_lines[i];
        if (isnan(line.x) || isnan(line.y)) { // No wall found
            walls_connected = false;
            continue;
        }

        float len = sqrtf(line.x*line.x + line.y*line.y);
        float theta_up = atan((WALL_HEIGHT - HEAD_HEIGHT)/len);
        float theta_down = atan(HEAD_HEIGHT/len);
        Seg this_wall = seg_from_absolute(vec(screen_x, height/2 - sin(theta_up)*height/2), vec(screen_x, height/2 + sin(theta_down)*height/2));

        int this_alpha = (int) interpolate(20.0f, maxf(width, height) / 2, len, 0.0f, 255);

        SDL_SetRenderDrawColor(r, UNPACK_COLOUR((WALL_COLOR - (WALL_COLOR & 0x01010100) * this_alpha + BACKGROUND)));

        SDL_RenderDrawLine(r, this_wall.a.x, this_wall.a.y, endp(this_wall).x, endp(this_wall).y);

        if (walls_connected) {
            SDL_Color this_c = COLOUR_TO_SDLCOLOR((WALL_COLOR - (WALL_COLOR & 0x01010100) * this_alpha + BACKGROUND));
            SDL_Color prev_c = COLOUR_TO_SDLCOLOR((WALL_COLOR - (WALL_COLOR & 0x01010100) * prev_alpha + BACKGROUND));
            SDL_FPoint tc = {0};
            SDL_Vertex vertices[6];
            Vec this_end = endp(this_wall);
            Vec prev_end = endp(prev_wall);
            vertices[0] = (SDL_Vertex) { .position = *(SDL_FPoint*) &this_wall.a, .color = this_c, .tex_coord = tc};
            vertices[1] = (SDL_Vertex) { .position = *(SDL_FPoint*) &this_end,    .color = this_c, .tex_coord = tc};
            vertices[2] = (SDL_Vertex) { .position = *(SDL_FPoint*) &prev_wall.a, .color = prev_c, .tex_coord = tc};
            vertices[3] = (SDL_Vertex) { .position = *(SDL_FPoint*) &prev_wall.a, .color = prev_c, .tex_coord = tc};
            vertices[4] = (SDL_Vertex) { .position = *(SDL_FPoint*) &prev_end,    .color = prev_c, .tex_coord = tc};
            vertices[5] = (SDL_Vertex) { .position = *(SDL_FPoint*) &this_end,    .color = this_c, .tex_coord = tc};
            sc(SDL_RenderGeometry(r, NULL, vertices, sizeof(vertices)/sizeof(vertices[0]), NULL, 0), "rendering a quad");
        }

        prev_alpha = this_alpha;
        prev_wall = this_wall;
        walls_connected = true;
        screen_x += delta_x;
    }

    draw_minimap(r);
    return;
}

int main(int argc, char *argv[]) {
    init_state();

    sc(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS), "Initiating SDL");

    SDL_Window* w = scp(SDL_CreateWindow("Rend", 0, 0, width, height, 0), "creating window");
    SDL_Renderer* r = scp(SDL_CreateRenderer(w, -1, SDL_RENDERER_ACCELERATED), "creating renderer");

    bool running = true;

    SDL_Event e;

    while(running) {
        while(SDL_PollEvent(&e)) {
            switch (e.type) {
                case SDL_WINDOWEVENT: {
                    if (e.window.event == SDL_WINDOWEVENT_CLOSE)
                        running = false;
                }; break;
                case SDL_MOUSEMOTION: {
                    s.center.x = (float) e.motion.x;
                    s.center.y = (float) e.motion.y;
                }; break;
                case SDL_MOUSEWHEEL: {
                    #define ROTATION_SCALE -10.0f
                    rotate_view(e.wheel.preciseY / ROTATION_SCALE);
                }; break;
                default:
                    break;
            }
        }
        update_sight_lines();
        clear(r);
        render(r);
        SDL_RenderPresent(r);
    }
    return 0;
}
